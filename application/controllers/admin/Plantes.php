<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Plantes extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();

		/* Load :: Common */
		$this->lang->load('admin/plantes');
		$this->load->model('admin/plantes_model');
		$this->load->model('admin/capteurs_model');
		
		/* Title Page :: Common */
		$this->page_title->push(lang('menu_plantes'));
		$this->data['pagetitle'] = $this->page_title->show();

		/* Breadcrumbs :: Common */
		$this->breadcrumbs->unshift(1, lang('menu_plantes'), 'admin/plantes');
	}


	public function index()
	{
		if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
		{
			redirect('auth/login', 'refresh');
		}
		elseif ($this->input->get('action') AND $this->input->get('id')) {
			$id = $this->input->get('id');
			$this->plantes_model->delete_plant($id);
			redirect('admin/plantes/', 'reload');
		}
		else
		{
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

			/* Get all users */
			$this->data['plantes'] = $this->plantes_model->get_plants();
			$this->data['capteur'] = $this->capteurs_model->get_capteurs_id();
			// foreach ($this->data['users'] as $k => $user)
			// {
			// 	$this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			// }

			/* Load Template */
			$this->template->admin_render('admin/plantes/index.php', $this->data);
			
			
		}
	}

	public function info($id)
	{
		
		
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();
			$this->data['tmp'] = $id;

			/* Get all users */
			$this->data['plante_info'] = $this->plantes_model->get_plants_info_id($id);
			// foreach ($this->data['users'] as $k => $user)
			// {
			// 	$this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			// }

			/* Load Template */
			$this->template->admin_render('admin/plantes/info.php', $this->data);
			
	}

	public function create()
	{
		$this->data['capteur'] = $this->capteurs_model->get_capteurs_id();
		$this->data['plantes_id'] = $this->plantes_model->get_plants_id();
		$this->data['plantes_champs'] = $this->plantes_model->get_id_nom_plants();
		$this->data['plantes_nom'] = $this->plantes_model->get_plants_nom();
		$this->data['breadcrumb'] = $this->breadcrumbs->show();





		$this->form_validation->set_rules('plantes_nom', 'lang:nom_plante', 'required');
		$this->form_validation->set_rules('plantes_type', 'lang:type_plante', 'required');
		$this->form_validation->set_rules('capteurs_id', 'lang:capteur_affecte', 'required');

		if ($this->form_validation->run() == TRUE)
		{
			if (isset($_POST) && ! empty($_POST))
			{
				$plantes_nom = strtolower($this->input->post('plantes_nom'));
				$plantes_type = strtolower($this->input->post('plantes_type'));
				$capteurs_id = strtolower($this->input->post('capteurs_id'));
				$this->plantes_model->add_plant($plantes_nom,$plantes_type,$capteurs_id);
				redirect('admin/plantes/', 'reload');
			}
		}
		$this->template->admin_render('admin/plantes/create.php', $this->data);
	}
}
