<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Capteurs extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();

		/* Load :: Common */
        $this->lang->load('admin/capteurs');
        $this->load->model('admin/capteurs_model');

		/* Title Page :: Common */
		$this->page_title->push(lang('menu_capteurs'));
		$this->data['pagetitle'] = $this->page_title->show();

		/* Breadcrumbs :: Common */
		$this->breadcrumbs->unshift(1, lang('menu_capteurs'), 'admin/capteurs');
	}


	public function index()
	{
		if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
		{
			redirect('auth/login', 'refresh');
		}
		else
		{
			/* Breadcrumbs */
			$this->data['breadcrumb'] = $this->breadcrumbs->show();

			/* Get all users */
            //$this->data['capteurs'] = $this->capteurs_model->users()->result();
            
            $this->data['capteurs'] = $this->capteurs_model->get_capteurs();

			/*foreach ($this->data['users'] as $k => $user)
			{
				$this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			}*/

			/* Load Template */
			$this->template->admin_render('admin/capteurs/index', $this->data);
		}
	}


	


	public function delete()
	{
		/* Load Template */
		$this->template->admin_render('admin/users/delete', $this->data);
	}


	public function edit($id)
	{
		//$id = $id;
		//$id = (int) $id;

		if ( ! $this->ion_auth->logged_in() OR ( ! $this->ion_auth->is_admin() && ! ($this->ion_auth->user()->row()->id == $id)))
		{
			redirect('auth', 'refresh');
		}

        /* Breadcrumbs */
        //$this->breadcrumbs->unshift(2, lang('menu_users_edit'), 'admin/users/edit');
		$this->breadcrumbs->unshift(2, lang('menu_capteurs_edit'), 'admin/capteurs/edit');
		$this->data['breadcrumb'] = $this->breadcrumbs->show();

		/* Data */
		$this->data['capteur'] = $this->capteurs_model->capteur($id);
		/*$groups        = $this->ion_auth->groups()->result_array();
		$currentGroups = $this->ion_auth->get_users_groups($id)->result();*/

		/* Validate form input */

		//$this->form_validation->set_rules('capteur_keepalive', 'lang:edit_user_validation_phone_label', 'required');

		/*$this->data['capteur'] = $this->capteurs_model->get_capteurs_id();
		$this->data['plantes_id'] = $this->plantes_model->get_plants_id();
		$this->data['plantes_champs'] = $this->plantes_model->get_id_nom_plants();
		$this->data['plantes_nom'] = $this->plantes_model->get_plants_nom();
		$this->data['breadcrumb'] = $this->breadcrumbs->show();*/

		$this->form_validation->set_rules('capteur_id', 'lang:capteur_id', 'required');
		$this->form_validation->set_rules('capteur_emplacement', 'lang:capteur_emplacement', 'required');

		if ($this->form_validation->run() == TRUE)
		{
			if (isset($_POST) && ! empty($_POST))
			{
				$capteur_id = strtolower($this->input->post('capteur_id'));
				$capteur_emplacement = strtolower($this->input->post('capteur_emplacement'));
				$this->capteurs_model->update_capteur($capteur_id,$capteur_emplacement);
				redirect('admin/capteurs/', 'reload');
			}
		}
		
		/*if (isset($_POST) && ! empty($_POST))
		{
			if ($this->_valid_csrf_nonce() === FALSE OR $id != $this->input->post('id'))
			{
				show_error($this->lang->line('error_csrf'));
			}


			if ($this->form_validation->run() == TRUE)
			{
				$data = array(
					'first_name' => $this->input->post('emplacement'),
					'last_name'  => $this->input->post('last_name'),
					'company'    => $this->input->post('company'),
					'phone'      => $this->input->post('phone')
				);

				
				
			}
		}*/

		// display the edit user form
		$this->data['csrf'] = $this->_get_csrf_nonce();

		// set the flash data error message if there is one
		$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

		// pass the user to the view
		//$this->data['user']          = $user;
		//$this->data['groups']        = $groups;
		//$this->data['currentGroups'] = $currentGroups;

		
		


		/* Load Template */
		$this->template->admin_render('admin/capteurs/edit', $this->data);
	}


	public function profile($id)
	{
		/* Breadcrumbs */
		$this->breadcrumbs->unshift(2, lang('menu_users_profile'), 'admin/groups/profile');
		$this->data['breadcrumb'] = $this->breadcrumbs->show();

		/* Data */
		$id = (int) $id;

		$this->data['user_info'] = $this->ion_auth->user($id)->result();
		foreach ($this->data['user_info'] as $k => $user)
		{
			$this->data['user_info'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
		}

		/* Load Template */
		$this->template->admin_render('admin/users/profile', $this->data);
	}


	public function _get_csrf_nonce()
	{
		$this->load->helper('string');
		$key   = random_string('alnum', 8);
		$value = random_string('alnum', 20);
		$this->session->set_flashdata('csrfkey', $key);
		$this->session->set_flashdata('csrfvalue', $value);

		return array($key => $value);
	}


	public function _valid_csrf_nonce()
	{
		if ($this->input->post($this->session->flashdata('csrfkey')) !== FALSE && $this->input->post($this->session->flashdata('csrfkey')) == $this->session->flashdata('csrfvalue'))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
}
