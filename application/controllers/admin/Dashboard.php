<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        /* Load :: Common */
        $this->load->helper('number');
        $this->load->model('admin/dashboard_model');
        $this->load->model('admin/capteurs_model');
        $this->load->model('admin/meteo_model');
        $this->load->model('admin/prevision_plante_model');
    }


	public function index()
	{
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            /* Title Page */
            $this->page_title->push(lang('menu_dashboard'));
            $this->data['pagetitle'] = $this->page_title->show();

            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /* Data */
            $this->data['count_users']       = $this->dashboard_model->get_count_record('users');
            $this->data['count_groups']      = $this->dashboard_model->get_count_record('groups');
            $this->data['disk_totalspace']   = $this->dashboard_model->disk_totalspace(DIRECTORY_SEPARATOR);
            $this->data['disk_freespace']    = $this->dashboard_model->disk_freespace(DIRECTORY_SEPARATOR);
            $this->data['disk_usespace']     = $this->data['disk_totalspace'] - $this->data['disk_freespace'];
            $this->data['disk_usepercent']   = $this->dashboard_model->disk_usepercent(DIRECTORY_SEPARATOR, FALSE);
            $this->data['memory_usage']      = $this->dashboard_model->memory_usage();
            $this->data['memory_peak_usage'] = $this->dashboard_model->memory_peak_usage(TRUE);
            $this->data['memory_usepercent'] = $this->dashboard_model->memory_usepercent(TRUE, FALSE);
            $this->data['resumer1'] = $this->dashboard_model->graph_json1();
            $this->data['resumer2'] = $this->dashboard_model->graph_json2();
            $this->data['capteur'] = $this->dashboard_model->get_capteur();
            //$this->data['metrique'] = $this->dashboard_model->graph_json();
            $this->data['keepalive'] = $this->dashboard_model->get_keepalive();
            $this->data['meteo'] = $this->meteo_model->get_meteo();
            $this->data['meteo_next'] = $this->meteo_model->get_meteo_2days();
            $plante_bi = $this->data['plante_bi'] = $this->prevision_plante_model->get_plante();
            $this->data['plante_type1'] = $this->dashboard_model->get_plante_type1();
            $this->data['plante_type2'] = $this->dashboard_model->get_plante_type2();
            $this->data['plante_type3'] = $this->dashboard_model->get_plante_type3();
            $this->data['prev_plant'] = $this->prevision_plante_model->get_prev_plante($plante_bi);
            $this->data['humidite'] = $this->dashboard_model->get_humidity();
            $this->data['day2'] = $this->dashboard_model->get_day_2j();
            $this->data['humidite7'] = $this->dashboard_model->get_humidity_7j();
            $this->data['temperature7'] = $this->dashboard_model->get_temperature_7j();
            $this->data['plantbysensor'] = $this->dashboard_model->plants_by_sensor();
            $this->data['lastdata'] = $this->dashboard_model->get_lastdata();


            /* TEST */
            //$this->data['url_exist']    = is_url_exist('http://www.domprojects.com');


            /* Load Template */
            $this->template->admin_render('admin/dashboard/index', $this->data);
        }
	}
}
