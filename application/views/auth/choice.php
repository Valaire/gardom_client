<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php      
  header('Location: ../admin');      
?>

<p><a href="<?php echo site_url('/'); ?>">Home</a></p>

<p><a href="<?php echo site_url('admin'); ?>">Admin</a></p>

<p><a href="<?php echo site_url('auth/logout'); ?>">Logout</a></p>

