<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

            <div class="content-wrapper">
                <section class="content-header">
                    <?php 
                        //var_dump($prev_plant[0]);
                        //$plante_bi[0]['id_gardom_plant']
                        /*foreach($plante_bi as $plante){
                           echo $plante['id_gardom_plant']."<br /><br />";
                        }*/

                        /*foreach($prev_plant as $prev)
                        {
                            var_dump($prev);
                        }*/
                        //echo $prev_plant[0]['id_gardom_plant'];
                        $set_type = array();
                        array_push($set_type,$plante_type1[0]['count(*)']);
                        array_push($set_type,$plante_type2[0]['count(*)']);
                        array_push($set_type,$plante_type3[0]['count(*)']);
                        switch ($meteo->weather[0]->description) {
                            case "orage et pluie":
                                $meteotmp = "10d@2x";
                                break;
                            case "légère pluie":
                                $meteotmp = "10d@2x";
                                break;
                            case "partiellement nuageux":
                                $meteotmp = "02d@2x";
                                break;
                            case "peu nuageux":
                                $meteotmp = "02d@2x";
                                break;
                            case "nuageux":
                                $meteotmp = "03d@2x";
                                break;
                            case "ciel dégagé":
                                $meteotmp = "01d@2x";
                                break;
                            case "couvert":
                                $meteotmp = "03d@2x";
                                break;
                            case "pluie modérée":
                                $meteotmp = "10d@2x";
                                break;
                            case "orage":
                                $meteotmp = "11d@2x";
                                break;
                            case "orage et forte pluie":
                                $meteotmp = "11d@2x";
                                break;
                        }
                        switch ($meteo_next->list[1]->weather[0]->description) {
                            case "orage et pluie":
                                $meteotmp1 = "10d@2x";
                                break;
                            case "légère pluie":
                                $meteotmp1 = "10d@2x";
                                break;
                            case "partiellement nuageux":
                                $meteotmp1 = "02d@2x";
                                break;
                            case "peu nuageux":
                                $meteotmp1 = "02d@2x";
                                break;
                            case "nuageux":
                                $meteotmp1 = "03d@2x";
                                break;
                            case "ciel dégagé":
                                $meteotmp1 = "01d@2x";
                                break;
                            case "couvert":
                                $meteotmp1 = "03d@2x";
                                break;
                            case "pluie modérée":
                                $meteotmp1 = "10d@2x";
                                break;
                            case "orage":
                                $meteotmp1 = "11d@2x";
                                break;
                            case "orage et forte pluie":
                                $meteotmp1 = "11d@2x";
                                break;
                            /*case "légère pluie"*/
                        }
                        switch ($meteo_next->list[2]->weather[0]->description) {
                            case "orage et pluie":
                                $meteotmp2 = "10d@2x";
                                break;
                            case "légère pluie":
                                $meteotmp2 = "10d@2x";
                                break;
                            case "partiellement nuageux":
                                $meteotmp2 = "02d@2x";
                                break;
                            case "peu nuageux":
                                $meteotmp2 = "02d@2x";
                                break;
                            case "nuageux":
                                $meteotmp2 = "03d@2x";
                                break;
                            case "ciel dégagé":
                                $meteotmp2 = "01d@2x";
                                break;
                            case "couvert":
                                $meteotmp2 = "03d@2x";
                                break;
                            case "pluie modérée":
                                $meteotmp2 = "10d@2x";
                                break;
                            case "orage":
                                $meteotmp2 = "11d@2x";
                                break;
                            case "orage et forte pluie":
                                $meteotmp2 = "11d@2x";
                                break;
                           /* case "légère pluie"*/
                        }
                        
                         ?>
                    <?php echo $pagetitle; ?>
                    <?php echo $breadcrumb;?>
                </section>

                <section class="content">
                    
                            
                        
                        <div class="row">
                            <div class="col-md-6">
                                <div class="box">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Température sur la dernière heure</h3>
                                        <div class="box-tools pull-right">
                                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        </div>
                                    </div>
                                    <div class="box-body">
                                        <div class="row">  
                                            <canvas id="line-chart" width="200" height="100"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                            <div class="col-md-6">
                                <div class="box">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Humidité sur la dernière heure</h3>
                                        <div class="box-tools pull-right">
                                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        </div>
                                    </div>
                                    <div class="box-body">
                                        <div class="row">  
                                            <canvas id="line-chart5" width="200" height="100"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div></div>
                        
                        <!--<div class="col-md-12">
                            <div class="box">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Températures sur la dernière heure</h3>
                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <canvas id="line-chart" width="200" height="100"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>-->

                        <script type="text/javascript">
                            new Chart(document.getElementById("line-chart"), {
                                type: 'line',
                                data: {
                                    labels: [<?PHP foreach($resumer1 as $labeldata){$retr = substr($labeldata['timestamp_metric'], 11); echo "'".$retr."',";} ?>],
                                    datasets: [{ 
                                        data: [<?PHP foreach($resumer1 as $temperature){echo $temperature['temperature_metric'].",";}?>],
                                        label: '<?PHP echo $capteur[0]['emplacement_sensor']; ?>',
                                        borderColor: "#3e95cd",
                                        fill: false
                                    }, { 
                                        data: [<?PHP foreach($resumer2 as $temperature){echo $temperature['temperature_metric'].",";} ?>],
                                        label: '<?PHP echo $capteur[1]['emplacement_sensor']; ?>',
                                        borderColor: "#8e5ea2",
                                        fill: false
                                    }
                                    ]
                                },
                                options: {
                                    title: {
                                    display: true,
                                    text: 'Métriques température sur la dernière heure',
                                    }
                                }
                            });
                        </script>

                        <script type="text/javascript">
                            new Chart(document.getElementById("line-chart5"), {
                                type: 'line',
                                data: {
                                    labels: [<?PHP foreach($resumer1 as $labeldata){$retr = substr($labeldata['timestamp_metric'], 11); echo "'".$retr."',";} ?>],
                                    datasets: [{ 
                                        data: [<?PHP foreach($resumer1 as $temperature){echo $temperature['humidity_metrique'].",";}?>],
                                        label: '<?PHP echo $capteur[0]['emplacement_sensor']; ?>',
                                        borderColor: "#3e95cd",
                                        fill: false
                                    }, { 
                                        data: [<?PHP foreach($resumer2 as $temperature){echo $temperature['humidity_metrique'].",";} ?>],
                                        label: '<?PHP echo $capteur[1]['emplacement_sensor']; ?>',
                                        borderColor: "#8e5ea2",
                                        fill: false
                                    }]
                                },
                                options: {
                                    title: {
                                    display: true,
                                    text: 'Métriques température sur la dernière heure',
                                    }
                                }
                            });
                        </script>
                    
                    
                    <div class="row">
                        
                    </div>
                    
                    </div>
                    

                    <div class="row">
                        <div class="col-md-4">
                            <div class="box">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Prévisions météorologique</h3>
                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <?PHP $meteo_prev = json_decode(json_encode($meteo_next), true);
                                            //var_dump($meteo_prev);  ?>
                                            
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="info-box bg-grey">
                                                    <span class="info-box-icon"><img src="<?PHP echo "http://localhost/gardomv2/assets/image/meteo/". $meteotmp .".png"; ?>" width="100" height="100" /></span> 
                                                    <div class="info-box-content">
                                                        <span class="info-box-text">Aujourd'hui : <?PHP echo $meteo->weather[0]->description; ?></span> 
                                                        <span class="info-box-number"><?PHP echo round($meteo->main->temp); ?> °C</span> 
                                                            <div class="progress"><div class="progress-bar" style="width: 100%;"></div>
                                                    </div>
                                                    <span class="progress-description">

                                                        Min : <?PHP echo round($meteo->main->temp_min); ?> °C | Max : <?PHP echo round($meteo->main->temp_max); ?> °C | Précipitations : 1.2 | Vent : <?PHP echo $meteo->wind->speed; ?> m/s
                                                    </span>
                                                </div>
                                            </div></div>
                                       
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="info-box bg-grey">
                                                    <span class="info-box-icon"><img src="<?PHP echo "http://localhost/gardomv2/assets/image/meteo/". $meteotmp1 .".png"; ?>" width="100" height="100" /></span> 
                                                    <div class="info-box-content">
                                                        <span class="info-box-text">Demain : <?PHP echo $meteo_next->list[1]->weather[0]->description; ?></span> 
                                                        <span class="info-box-number"><?PHP echo round($meteo_next->list[1]->main->temp); ?> °C</span> 
                                                            <div class="progress"><div class="progress-bar" style="width: 100%;"></div>
                                                    </div>
                                                    <span class="progress-description">
                                                        
                                                        Min : <?PHP echo round($meteo_next->list[1]->main->temp_min); ?> °C | Max : <?PHP echo round($meteo_next->list[1]->main->temp_max); ?> °C | Précipitations : <?PHP echo $meteo_prev['list'][1]['rain']['3h']; ?>
                                                    </span>
                                                </div>
                                            </div></div>

                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="info-box bg-grey">
                                                    <span class="info-box-icon"><img src="<?PHP echo "http://localhost/gardomv2/assets/image/meteo/". $meteotmp2 .".png"; ?>" width="100" height="100" /></i></span> 
                                                    <div class="info-box-content">
                                                        <span class="info-box-text">Après-demain : <?PHP echo $meteo_next->list[2]->weather[0]->description; ?></span> 
                                                        <span class="info-box-number"><?PHP echo round($meteo_next->list[2]->main->temp); ?> °C</span> 
                                                            <div class="progress"><div class="progress-bar" style="width: 100%;"></div>
                                                    </div>
                                                    <span class="progress-description">
                                                        
                                                        Min : <?PHP echo round($meteo_next->list[2]->main->temp_min); ?> °C | Max : <?PHP echo round($meteo_next->list[2]->main->temp_max); ?> °C | Précipitations : <?PHP echo $meteo_prev['list'][2]['rain']['3h']; ?>
                                                    </span>
                                                </div></div>
                                            </div>
                                        </div>    
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-4">
                            <div class="box">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Proportion de légumes et fruits</h3>
                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                    </div>
                                </div>
                                <div class="box-body">
                                        
                                    <canvas id="chart-area" width="200" height="100"></canvas>
                                        
                                </div>
                                
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="box">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Etat des capteurs</h3>
                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                    </div>
                                </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-14">
                                <?php foreach ($keepalive as $key)
                                if($key['keepalive_sensor'] == '1') {?>
            
                                    <div class="col-md-6 col-sm-6 col-xs-12"><a href="capteurs/" >
                                        <div class="info-box">
                                            <span class="info-box-icon bg-green"><i class="fa fa-check"></i></span>
                                            <div class="info-box-content">
                                            <span class="info-box-text"><?php echo htmlspecialchars("Capteur ".$key['id_sensor']." en ligne", ENT_QUOTES, 'UTF-8');  ?></span>
                                            <span class="info-box-number"><?php echo htmlspecialchars($key['emplacement_sensor'], ENT_QUOTES, 'UTF-8'); ?></span>
                                            </div>
                                        </div>
                                    </div></a>

                                <?PHP }else{ ?>
                                    <div class="col-md-6 col-sm-6 col-xs-12"><a href="capteurs/" >
                                        <div class="info-box">
                                            <span class="info-box-icon bg-red"><i class="icon fa fa-ban"></i></span>
                                            <div class="info-box-content">
                                            <span class="info-box-text"><?php echo htmlspecialchars("Capteur ".$key['id_sensor']." hors ligne", ENT_QUOTES, 'UTF-8'); ?></span>
                                            <span class="info-box-number"><?php echo htmlspecialchars($key['emplacement_sensor'], ENT_QUOTES, 'UTF-8'); ?></span>
                                            </div>
                                        </div>
                                    </div></a>


                                <?PHP } ?>
                                </div>
                                </div>
                                </div>
                                
                                </div>
                                </div>
                            </div>
                            <script type="text/javascript">
                                let unix_timestamp = <?PHP echo $meteo->sys->sunrise; ?>;
                                let unix_timestamp1 = <?PHP echo $meteo->sys->sunset; ?>;
                                // Create a new JavaScript Date object based on the timestamp
                                // multiplied by 1000 so that the argument is in milliseconds, not seconds.
                                var dates = new Date(unix_timestamp * 1000);
                                var dates1 = new Date(unix_timestamp1 * 1000);
                                // Hours part from the timestamp
                                var hours = dates.getHours();
                                var hours1 = dates1.getHours();
                                // Minutes part from the timestamp
                                var minutes = "0" + dates.getMinutes();
                                var minutes1 = "0" + dates1.getMinutes();
                                // Seconds part from the timestamp
                                var seconds = "0" + dates.getSeconds();
                                var seconds1 = "0" + dates1.getSeconds();

                                // Will display time in 10:30:23 format
                                var formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
                                var formattedTime1 = hours1 + ':' + minutes1.substr(-2) + ':' + seconds1.substr(-2);

                                var formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
                                var formattedTime1 = hours1 + ':' + minutes1.substr(-2) + ':' + seconds1.substr(-2);

                                console.log(formattedTime,formattedTime1);
                            </script>
                            <?PHP //$sunrise = echo "<script type='text/javascript'>var formattedTime</script>?>
                            
                            <script type="text/javascript">
                        data1 = {
                            datasets: [{
                                data: [<?PHP foreach($set_type as $plante_type){echo $plante_type.",";} ?>],
                                backgroundColor: [
                                    'rgba(255, 99, 132)',
                                    'rgba(54, 162, 235)',
                                    'rgba(255, 206, 86)'
                                ]
                            }],
                            // These labels appear in the legend and in the tooltips when hovering different arcs
                            labels: [
                                'Légumes',
                                'Fruits',
                                'Plantes'
                            ]
                            };
                            var ctx = document.getElementById('chart-area');
                            var myDoughnutChart = new Chart(ctx, {
                                type: 'doughnut',
                                data: data1
                            });                                 
                            </script>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="box">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Humidité moyenne sur les 7 derniers jours</h3>
                                        <div class="box-tools pull-right">
                                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        </div>
                                    </div>
                                    <div class="box-body">
                                            
                                        <canvas id="chart-area1" width="200" height="100"></canvas>
                                            
                                    </div>
                                    
                                </div>
                            </div>
                        
                            <div class="col-md-4">
                                <div class="box">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Température moyenne sur les 7 derniers jours</h3>
                                        <div class="box-tools pull-right">
                                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        </div>
                                    </div>
                                    <div class="box-body">
                                            
                                        <canvas id="chart-area2" width="200" height="100"></canvas>
                                            
                                    </div>
                                    
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="box">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Précipitations sur les 2 prochains jours</h3>
                                        <div class="box-tools pull-right">
                                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        </div>
                                    </div>
                                    <div class="box-body">
                                            
                                        <canvas id="chart-area3" width="200" height="100"></canvas>
                                            
                                    </div>
                                    
                                </div>
                            </div>
                            
                            <div class="col-md-4">
                                <div class="box">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Plantes par capteurs</h3>
                                        <div class="box-tools pull-right">
                                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        </div>
                                    </div>
                                    <div class="box-body">
                                        <?PHP foreach ($keepalive as $key){echo "<br /><b>Capteur : ".$key['emplacement_sensor']."</b><br />";foreach($plantbysensor as $pbs){if($key['emplacement_sensor'] == $pbs['emplacement_sensor']){echo "<a href='plantes/info/".$pbs['id_gardom_plant']."'><img src='http://localhost/gardomv2/assets/image/plante/".$pbs['name_plant'].".jpg' width=70 height=70 /></a>";}}} ?>
                                        
                                            
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        
                        <script type="text/javascript">
                            data2 = {
                                datasets: [{
                                    data: [<?PHP foreach($humidite7 as $hum){echo $hum['AVG(`humidity_metrique`)'].",";} ?>],
                                    barPercentage: 0.5,
                                    label: 'Humidité',
                                    barThickness: 30,
                                    maxBarThickness: 30,
                                    minBarLength: 2,
                                    backgroundColor: [
                                        'rgba(54, 162, 235)',
                                        'rgba(54, 162, 235)',
                                        'rgba(54, 162, 235)',
                                        'rgba(54, 162, 235)',
                                        'rgba(54, 162, 235)',
                                        'rgba(54, 162, 235)',
                                        'rgba(54, 162, 235)'
                                    ]
                                }],
                                // These labels appear in the legend and in the tooltips when hovering different arcs
                                labels: [<?PHP foreach($humidite7 as $humm){echo substr($humm['SUBSTRING(`timestamp_metric`,6,5)'],-2).",";}  ?>

                                ]
                                };
                                var ctx = document.getElementById('chart-area1');
                                var myDoughnutChart = new Chart(ctx, {
                                    type: 'bar',
                                    data: data2,
                                });                                 
                            </script>
                            <script type="text/javascript">
                            data3 = {
                                datasets: [{
                                    data: [<?PHP foreach($temperature7 as $tem){echo $tem['SUBSTRING(avg(`temperature_metric`),1,4)'].",";} ?>],
                                    label: 'Température',
                                    fill: true,
                                    backgroundColor: "orange",
                                    pointBackgroundColor: "red"
                                }],
                                // These labels appear in the legend and in the tooltips when hovering different arcs
                                labels: [<?PHP foreach($temperature7 as $tem){echo substr($tem['SUBSTRING(`timestamp_metric`,6,5)'],-2).",";} ?>

                                ]
                                };
                                var ctx = document.getElementById('chart-area2');
                                var myDoughnutChart = new Chart(ctx, {
                                    type: 'line',
                                    data: data3
                                });                                 
                            </script>

                            <script type="text/javascript">
                            data4 = {
                                datasets: [{
                                    data: [<?PHP echo $meteo_prev['list'][1]['rain']['3h'].",";echo $meteo_prev['list'][2]['rain']['3h']; ?>],
                                    barPercentage: 0.5,
                                    label: 'Précipitations',
                                    barThickness: 30,
                                    maxBarThickness: 30,
                                    minBarLength: 2,
                                    fill: true,
                                    backgroundColor: "rgba(63,127,191,1)",
                                    pointBackgroundColor: "red"
                                }],
                                // These labels appear in the legend and in the tooltips when hovering different arcs
                                labels: [<?PHP foreach($day2 as $d2){echo substr($d2['SUBSTRING(`timestamp_metric`,6,5)'],-2).",";} ?>
                                ]
                                };
                                var ctx = document.getElementById('chart-area3');
                                var myDoughnutChart = new Chart(ctx, {
                                    type: 'bar',
                                    data: data4
                                });                                 
                            </script>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Ces plantes requièrent votre attention</h3>
                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                    </div>
                                </div>
                                <center><h4>Selon les derniers relevés et les estimations des prochains jours, les plantes ci-dessous ont un risque de mal supporter les conditions climatiques</h4></center>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="box">
                                                <div class="box-header with-border">
                                                    <h3 class="box-title">Aujourd'hui</h3>
                                                    <div class="box-tools pull-right">
                                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                                    </div>
                                                </div>
                                                <div class="box-body"><center><h4></h4></center>
                                                            <?PHP //include('http://localhost/gardomv2/application/views/admin/dashboard/prev_plant');
                                                            $this->load->view('admin/dashboard/prev_planttod');  ?>
                                                            
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="box">
                                                <div class="box-header with-border">
                                                    <h3 class="box-title">Demain</h3>
                                                    <div class="box-tools pull-right">
                                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                                    </div>
                                                </div>
                                                <div class="box-body"><center><h4 plantations</h4></center>

                                                            <?PHP //include('http://localhost/gardomv2/application/views/admin/dashboard/prev_plant');
                                                            $this->load->view('admin/dashboard/prev_planttom');  ?>
                                                            
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="box">
                                                <div class="box-header with-border">
                                                    <h3 class="box-title">Après-demain</h3>
                                                    <div class="box-tools pull-right">
                                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                                    </div>
                                                </div>
                                                <div class="box-body">
                                                    <center><h4></h4></center>

                                                    <?PHP //include('http://localhost/gardomv2/application/views/admin/dashboard/prev_plant');
                                                    $this->load->view('admin/dashboard/prev_plantato');  ?>
                                                   
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                </section>
            </div>