<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

            <div class="content-wrapper">
                <section class="content-header">
                    <?php echo $pagetitle; ?>
                    <?php echo $breadcrumb; ?>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-14">
                             <div class="box">
                                <div class="box-header with-border">
                                    <b><h2 class="box-title"><?PHP echo $plante_info[0]['name_gardom_plant'] ?></h2></b>
                                </div>
                                <div class="box-body">
                                    <?php //echo $message;?>
                           
                                    
                                    

                                    <div class="col-md-6">
                                        <!-- Custom Tabs -->
                                        <div class="nav-tabs-custom">
                                            <ul class="nav nav-tabs">
                                            <li class="active"><a href="#tab_1" data-toggle="tab">Informations plante</a></li>
                                            <li><a href="#tab_2" data-toggle="tab">Température optimales</a></li>

                                            
                                            </ul>
                                            <div class="tab-content">
                                            <div class="tab-pane active" id="tab_1">

                                                <b>Conseils plantation : </b><br /><br />
                                                <?PHP echo $plante_info[0]['sowinstruction_gardom_plant']; ?><br /><br />
                                                
                                                <?PHP echo $plante_info[0]['spaceinstruction_gardom_plant']; ?><br /><br />
                                                <?PHP echo $plante_info[0]['harvestinstruction_gardom_plant']; ?><br /><br />
                                                <?PHP echo $plante_info[0]['compatibleplant_gardom_plant']; ?><br /><br />
                                            </div>
                                            <!-- /.tab-pane -->
                                            <div class="tab-pane" id="tab_2">
                                                <b>Température minimale : </b><?PHP echo $plante_info[0]['tempraturemin_gardom_plant']; ?> °C<br /><br /><br />

                                                <b>Température maximale : </b><?PHP echo $plante_info[0]['temperaturemax_gardom_plant']; ?> °C<br /><br /><br />

                                                <b>Humidité minimale : </b><?PHP echo $plante_info[0]['humiditemin_gardom_plant']; ?> %<br /><br /><br />

                                                <b>Humidité maximale : </b><?PHP echo $plante_info[0]['humiditemax_gardom_plant']; ?> %<br /><br /><br />
                                            </div>
                                            <!-- /.tab-pane -->
                                            
                                            <!-- /.tab-pane -->
                                            </div>
                                            <!-- /.tab-content -->
                                        </div>
                                        <!-- nav-tabs-custom -->
                                        </div><br /><br />
                                        <?PHP echo "<img src='http://localhost/gardomv2/assets/image/plante/".$plante_info[0]['name_gardom_plant'].".jpg' width=250 height=250 />"; ?>
                                    

                                </div>
                            </div>
                         </div>
                    </div>
                </section>
            </div>
