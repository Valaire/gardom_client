<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

            <div class="content-wrapper">
                <section class="content-header">
                    <?php echo $pagetitle; ?>
                    <?php echo $breadcrumb; ?>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                             <div class="box">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><?php echo lang('ajouter_plante'); ?></h3>
                                </div>
                                <div class="box-body">
                                    <?php echo form_open(current_url(), array('class' => 'form-horizontal', 'id' => 'form-create_plant')); 
                                    //echo($plantes_nom[1]['name_gardom_plant']);
                                    foreach($plantes_nom as $option)
                                    {
                                        $options[$option['name_gardom_plant']] = $option['name_gardom_plant'];
                                    }
                                    foreach($capteur as $option1)
                                    {
                                        $options1[$option1['id_sensor']] = $option1['id_sensor'];
                                    }
                                    //var_dump($capteur);
                                    //echo form_dropdown('shirts', $options); 

                                    //var_dump($option);
                                    //var_dump($options);

                                    $plantes_type = array('Legume'=>'Legume', 'Fruit'=>'Fruit', 'Plante'=>'Plante');
                                    //var_dump($plantes_type);
                                    //var_dump($plantes_nom[1]['name_gardom_plant']);
                                    ?>
                                        <div class="form-group">
                                            <?php echo lang('nom_plante', 'nom_plante', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">
                                                <?PHP echo form_dropdown('plantes_nom', $options); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('type_plante', 'plantes_type', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">
                                                <?PHP echo form_dropdown('plantes_type', $plantes_type); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('capteur_affecte', 'capteur_affecte', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">
                                                <?php echo form_dropdown('capteurs_id', $options1); ?>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                                <div class="btn-group">
                                                    <?php echo form_button(array('type' => 'add', 'class' => 'btn btn-primary btn-flat', 'content' => lang('action_submit'))); ?>
                                                    <?php echo anchor('admin/plantes', lang('action_cancel'), array('class' => 'btn btn-default btn-flat')); ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php echo form_close(); ?>
                                </div>
                                <!--<div class="box-body">
                                    <?php /*echo form_open(current_url(), array('class' => 'form-horizontal', 'id' => 'form-create_plant')); ?>
                                        <div class="form-group">
                                            <?php echo lang('nom_plante', 'nom_plante', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">
                                                <?php echo form_input($nom_plante);?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('type_plante', 'type_plante', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">
                                                <?php echo form_input($type_plante);?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('capteur_affecte', 'capteur_affecte', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">
                                                <?php echo form_input($capteur_affecte);?>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                                <div class="btn-group">
                                                    <?php echo form_button(array('type' => 'add', 'class' => 'btn btn-primary btn-flat', 'content' => lang('action_submit'))); ?>
                                                    <?php echo anchor('admin/plantes', lang('action_cancel'), array('class' => 'btn btn-default btn-flat')); ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php echo form_close();*/?>
                                </div>-->
                                <?PHP /* ?>
                                <div class="box-body">
                                    <form input
                                <?php 
                                $options1 = array();
                                $options2 = array();
                                $options3 = array();
                                foreach($plantes_champs as $options){array_push($options1,$options);}
                                foreach($plantes_champs as $options){array_push($options2,$options);}

                                var_dump($options1);
                                
		                        echo form_open(current_url(), array('class' => 'form-horizontal', 'id' => 'form-create_plant')); ?>
                                        <div class="form-group">
                                            <?php echo lang('nom_plante', 'nom_plante', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">
                                                <?php echo form_dropdown('nom_plante',$options1);?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('type_plante', 'type_plante', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">
                                                <?php echo form_dropdown('type_plante',$type_plante);
                                                
                                                //form_dropdown($data = '', $options = array(), $selected = array(), $extra = '')?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('capteur_affecte', 'capteur_affecte', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">
                                                <?php echo form_dropdown('capteur_affecte',$capteur_affecte);
                                                
                                                //form_dropdown($data = '', $options = array(), $selected = array(), $extra = '')?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                                <div class="btn-group">
                                                    <?php echo form_button(array('type' => 'add', 'class' => 'btn btn-primary btn-flat', 'content' => lang('action_submit'))); ?>
                                                    <?php echo anchor('admin/plantes', lang('action_cancel'), array('class' => 'btn btn-default btn-flat')); ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php echo form_close(); */ ?>
                                </div>
                            </div>
                         </div>
                    </div>
                </section>
            </div>
