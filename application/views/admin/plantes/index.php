<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="content-wrapper">
    <section class="content-header">
        <?php echo "$pagetitle"; ?>
        <?php echo $breadcrumb; ?>
    </section>
    
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">

            <!-- <div class="col-md-12"> -->
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo anchor('admin/plantes/create', '<i class="fa fa-plus"></i> '. lang('ajouter_plante'), array('class' => 'btn btn-block btn-primary btn-flat')); ?></h3>
            </div>
            <div class="box-body">
                <?php foreach ($plantes as $plante){?>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="box">
                        <div class="info-box">
                            <span class=""><a href="<?PHP echo "plantes/info/".$plante['id_gardom_plant'].""; ?>"><img src="<?PHP echo "http://localhost/gardomv2/assets/image/plante/".$plante['name_plant'].".jpg"; ?>" width="100" height="100" /></a><class="fa fa-check"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text"><?php echo $plante['name_plant'] ?></span>
                                <span class="info-box-number"><?php echo $plante['type_plant']?></span>
                                <?php echo anchor('admin/plantes/?action=delete&id='.$plante['id_plant'], lang('action_delete'));?>
                            </div>
                        </div>
                    </div>
                </div>  
                <?php } ?>
                
            </div>
        </div>
    </section>
</div>
