<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

            <div class="content-wrapper">
                <section class="content-header">
                    <?php echo $pagetitle; ?>
                    <?php echo $breadcrumb; ?>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                             <div class="box">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><?php echo lang('capteur_creer_capteur'); ?></h3>
                                </div>
                                <div class="box-body">
                                    <?php echo form_open(current_url(), array('class' => 'form-horizontal', 'id' => 'form-create_plant')); 
                                    //echo($plantes_nom[1]['name_gardom_plant']);
                                    /*foreach($plantes_nom as $option)
                                    {
                                        $options[$option['name_gardom_plant']] = $option['name_gardom_plant'];
                                    }
                                    foreach($capteur as $option1)
                                    {
                                        $options1[$option1['id_sensor']] = $option1['id_sensor'];
                                    }
                                    //var_dump($capteur);
                                    //echo form_dropdown('shirts', $options); 

                                    //var_dump($option);
                                    //var_dump($options);

                                    $plantes_type = array('Legume'=>'Legume', 'Fruit'=>'Fruit', 'Plante'=>'Plante');*/
                                    //var_dump($plantes_type);
                                    //var_dump($capteur[0]['id_sensor']);
                                    ?>
                                        <div class="form-group">
                                            <?php echo lang('', 'capteur_id', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">
                                                <?PHP echo form_hidden('capteur_id',$capteur[0]['id_sensor']); ?>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <?php echo lang('capteur_emplacement', 'capteur_emplacement', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">
                                                <?PHP echo form_input('capteur_emplacement'); ?>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                                <div class="btn-group">
                                                    <?php echo form_button(array('type' => 'add', 'class' => 'btn btn-primary btn-flat', 'content' => lang('action_submit'))); ?>
                                                    <?php echo anchor('admin/capteurs', lang('action_cancel'), array('class' => 'btn btn-default btn-flat')); ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php echo form_close(); ?>
                                </div>
                                
                                </div>
                            </div>
                         </div>
                    </div>
                </section>
            </div>
