<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

            <div class="content-wrapper">
                <section class="content-header">
                    <?php echo "$pagetitle"; ?>
                    <?php echo $breadcrumb; ?>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                             <div class="box">
                         

                                <div class="box-body">
                                    <table class="table table-striped table-hover">
										<thead>
											<tr>
												<th><?php echo lang('capteur_id');?></th>
												<th><?php echo lang('capteur_emplacement');?></th>
                                                <th><?php echo lang('capteur_alive');?></th>
                                                <th><?php echo lang('capteur_action');?></th>
											</tr>
                                        </thead>
                                        <?php foreach ($capteurs as $key){$id = $key['id_sensor'];?>
											<tr>
												<td><?php echo htmlspecialchars($key['id_sensor'], ENT_QUOTES, 'UTF-8'); ?></td>
												<td><?php echo htmlspecialchars($key['emplacement_sensor'], ENT_QUOTES, 'UTF-8'); ?></td>
                                                <td><?php if($key['keepalive_sensor'] == 1) { echo htmlspecialchars("En ligne", ENT_QUOTES, 'UTF-8'); } else { echo htmlspecialchars("Hors ligne", ENT_QUOTES, 'UTF-8'); } ?></td>
                                                <td>
													<?php echo anchor('admin/capteurs/edit/'.$id, lang('actions_edit')); ?>
													<?php //echo anchor('admin/capteurs/profile/'.$key['id_sensor'], lang('actions_see')); ?>
												</td>
                                            </tr>
                                        <?PHP } ?>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                    
                            <section class="content-header">
                                <h1>
                                    <i class="fa "></i> Historique capteur
                                    <small></small>
                                </h1>
                            </section>
                            <section class="content">
                                        <div class="row">
                                            <form action="http://localhost/Admin-Panel-User-Management-using-CodeIgniter/login-history" method="POST" id="searchList">
                                                <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 form-group">
                                                    <div class="input-group">
                                                        <input type="text" name="daterange" value="01/01/2018 - 01/15/2018" />
                                                    </div>
                                                </div>

                                                <!--<div class="col-lg-1 col-md-1 col-sm-6 col-xs-6 form-group">
                                                <button type="submit" class="btn btn-md btn-primary btn-block searchList pull-right"><i class="fa fa-search" aria-hidden="true"></i></button> 
                                                </div>-->
                                            </form>
                                            <canvas id="chart-canvas"></canvas>
                                        </div>
                                        </div>
                            </section>

                            <!--<section class="content">
                                        <div class="row">
                                            <form action="http://localhost/Admin-Panel-User-Management-using-CodeIgniter/login-history" method="POST" id="searchList">
                                                <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 form-group">
                                                    <div class="input-group">
                                                        <input type="text" name="daterange" value="01/01/2018 - 01/15/2018" />
                                                    </div>
                                                </div>

                                                <div class="col-lg-1 col-md-1 col-sm-6 col-xs-6 form-group">
                                                <button type="submit" class="btn btn-md btn-primary btn-block searchList pull-right"><i class="fa fa-search" aria-hidden="true"></i></button> 
                                                </div>
                                            </form>
                                            <canvas id="chart-canvas1"></canvas>
                                        </div>
                                        </div>
                            </section>-->
                            
                            <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
                            <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
                            <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
                            


                            <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
                            <script src="<?php echo base_url('assets/js/graph.js'); ?>"></script>
                            
                            
                           
                        </div>
                    </div>
                </section>
            </div>
