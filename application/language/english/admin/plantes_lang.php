<?php
/**
 * @package	CodeIgniter
 * @author	domProjects Dev Team
 * @copyright   Copyright (c) 2015, domProjects, Inc. (http://domProjects.com/)
 * @license http://opensource.org/licenses/MIT	MIT License
 * @link    http://domProjects.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['id_plante']              = 'ID';
$lang['ajouter_plante']         = 'Ajouter une plante';
$lang['nom_plante']             = 'Nom de la plante';
$lang['type_plante']            = 'Type de plante';
$lang['capteur_affecte']        = 'Capteur affecté';
//$lang['id_sensor']              = 'Plante';
$lang['action']                 = 'Actions';
$lang['action_edit']            = 'Editer ';
$lang['action_delete']          = 'Supprimer';
$lang['action_cancel']          = 'Annuler';
$lang['action_submit']          = 'Ajouter';
