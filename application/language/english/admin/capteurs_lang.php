<?php
/**
 * @package	CodeIgniter
 * @author	domProjects Dev Team
 * @copyright   Copyright (c) 2015, domProjects, Inc. (http://domProjects.com/)
 * @license http://opensource.org/licenses/MIT	MIT License
 * @link    http://domProjects.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['capteur_creer_capteur']     = 'Modifier l\'emplacement du capteur';

$lang['capteur_id']                = 'Identifiant capteur';
$lang['capteur_alive']             = 'Etat du capteur';  
$lang['capteur_action']            = 'Action';
$lang['capteur_emplacement']       = 'Emplacement capteur';

$lang['action_submit']            = 'Valider';
$lang['action_cancel']            = 'Annuler';