<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Plantes_model extends CI_Model {

    private $db1;

    public function __construct()
    {
        parent::__construct();
          $db1 = $this->load->database('otherdb', TRUE);
    }


    public function get_plants()
    {
        $sql = $this->db->query('SELECT * FROM gardom_client.plant');
        return $sql->result_array();
    }

    public function get_plants_info_id($id)
    {
        $sql = $this->db->query('SELECT * FROM gardom_client.gardom_plant WHERE id_gardom_plant = '.$id.'');
        return $sql->result_array();
    }


    public function get_id_nom_plants()
    {
        $sql = $this->db->query('SELECT id_gardom_plant,name_gardom_plant FROM gardom_client.gardom_plant');
        return $sql->result_array();
    }


    public function get_plants_nom()
    {
        $sql = $this->db->query('SELECT name_gardom_plant FROM gardom_client.gardom_plant');
        return $sql->result_array();
    }

    public function get_plants_id()
    {
        $sql = $this->db->query('SELECT id_gardom_plant FROM gardom_client.gardom_plant');
        return $sql->result_array();;
    }

    public function add_plant($plantes_nom,$plantes_type,$capteurs_id)
    {
      $sql = $this->db->select('id_plant');
      $this->db->order_by("id_plant desc");
      $query = $this->db->get('plant',1);
      foreach ($query->result() as $row){
        echo $row->id_plant;
      }
      $id = $row->id_plant+1;
      echo '<br />'.$id;

      $sql1 = $this->db->select('id_gardom_plant')->from('gardom_plant')->where(array('name_gardom_plant' => $plantes_nom ));
      $query1 = $this->db->get();
      //print_r($query);
      foreach ($query1->result() as $row1){
        echo $row1->id_gardom_plant;
      }

      //$id = $sql[0]['id_gardom_plant'];

      $data = array(
        'name_plant' => $plantes_nom,
        'type_plant' => $plantes_type,
        'id_gardom_plant' => $row1->id_gardom_plant
      );
      $data1 = array(
        'id_plant' => $id,
        'id_sensor' => $capteurs_id,
      );
      $this->db->insert('gardom_client.plant', $data);
      $this->db->insert('gardom_client.avoir', $data1);
    }

    public function delete_plant($id)
    {
      $this->db->delete('gardom_client.avoir', array('id_plant' => $id));
      $this->db->delete('gardom_client.plant', array('id_plant' => $id));
    }
}
