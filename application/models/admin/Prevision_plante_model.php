<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prevision_plante_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function get_plante()
    {
        $sql = $this->db->query('SELECT * FROM gardom_client.plant');

        return $sql->result_array();
    }

    public function get_prev_plante($plante_bi)
    {
        $dataset = array();

        foreach($plante_bi as $id)
        {
            $sql = $this->db->select('gardom_plant.id_gardom_plant, gardom_plant.tempraturemin_gardom_plant, gardom_plant.temperaturemax_gardom_plant, gardom_plant.humiditemin_gardom_plant, gardom_plant.humiditemax_gardom_plant, plant.name_plant')->from('plant,gardom_plant')->where(array('plant.id_gardom_plant' => $id['id_gardom_plant'], 'gardom_plant.id_gardom_plant' => $id['id_gardom_plant']));
            $query = $this->db->get();
            $data = $query->result();
            
            array_push($dataset,$data);
        }

        return $dataset;
    }

}