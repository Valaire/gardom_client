<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }


    public function get_count_record($table)
    {
        $query = $this->db->count_all($table);

        return $query;
    }

    public function graph_json1()
    {
     /* 'db' reference la bdd configuree dans database.php
     query() est une fonction propre a CI
     result() est aussi une fonction propre a CI
     */
     $sql = $this->db->query('SELECT * FROM (SELECT * FROM gardom_client.metric WHERE id_sensor = "10001" ORDER BY `timestamp_metric` DESC LIMIT 13) sub ORDER BY `timestamp_metric` ASC');
     //$sql = $this->db->query('SELECT capteur_id,temperature FROM gardom_client.metriques WHERE capteur_id = (SELECT DISTINCT capteur_id FROM gardom_client.metriques LIMIT 1)');
     return $sql->result_array();
    }

    public function graph_json2()
    {
     /* 'db' reference la bdd configuree dans database.php
     query() est une fonction propre a CI
     result() est aussi une fonction propre a CI
     */
    $sql = $this->db->query('SELECT * FROM (SELECT * FROM gardom_client.metric WHERE id_sensor = "10002" ORDER BY `timestamp_metric` DESC LIMIT 13) sub ORDER BY `timestamp_metric` ASC');
     //$sql = $this->db->query('SELECT capteur_id,temperature FROM gardom_client.metriques WHERE capteur_id = (SELECT DISTINCT capteur_id FROM gardom_client.metriques LIMIT 1)');
     return $sql->result_array();
    }

    public function get_capteur()
    {
        $sql = $this->db->query('SELECT id_sensor,emplacement_sensor FROM gardom_client.sensor');
        //$sql = $this->db->query('SELECT capteur_id,temperature FROM gardom_client.metriques WHERE capteur_id = (SELECT DISTINCT capteur_id FROM gardom_client.metriques LIMIT 1)');
        return $sql->result_array();
    }

    public function get_plante_type1()
    {
        $sql = $this->db->query("SELECT count(*) FROM plant WHERE type_plant = 'legume'");
        return $sql->result_array();
    }

    public function get_plante_type2()
    {
        $sql = $this->db->query("SELECT count(*) FROM plant WHERE type_plant = 'fruit'");
        return $sql->result_array();
    }

    public function get_plante_type3()
    {
        $sql = $this->db->query("SELECT count(*) FROM plant WHERE type_plant = 'plante'");
        return $sql->result_array();
    }

    public function get_keepalive()
    {
        $sql = $this->db->query('SELECT id_sensor,keepalive_sensor,emplacement_sensor FROM gardom_client.sensor');
        //$sql = $this->db->query('SELECT capteur_id,temperature FROM gardom_client.metriques WHERE capteur_id = (SELECT DISTINCT capteur_id FROM gardom_client.metriques LIMIT 1)');
        return $sql->result_array();
    }

    public function get_humidity()
    {
        $sql = $this->db->query('SELECT DISTINCT SUBSTRING(`timestamp_metric`,9,2) FROM `metric` ORDER BY SUBSTRING(`timestamp_metric`,9,2) ASC');
        //$sql = $this->db->query('SELECT capteur_id,temperature FROM gardom_client.metriques WHERE capteur_id = (SELECT DISTINCT capteur_id FROM gardom_client.metriques LIMIT 1)');
        return $sql->result_array();
    }

    public function get_humidity_7j()
    {
        $sql = $this->db->query('SELECT AVG(`humidity_metrique`),SUBSTRING(`timestamp_metric`,6,5) FROM `metric` WHERE SUBSTRING(`timestamp_metric`,6,5) = SUBSTRING(`timestamp_metric`,6,5) GROUP BY SUBSTRING(`timestamp_metric`,6,5) ORDER BY SUBSTRING(`timestamp_metric`,6,5) DESC LIMIt 7');
        //$sql = $this->db->query('SELECT capteur_id,temperature FROM gardom_client.metriques WHERE capteur_id = (SELECT DISTINCT capteur_id FROM gardom_client.metriques LIMIT 1)');
        return $sql->result_array();
    }

    public function get_day_2j()
    {
        $sql = $this->db->query('SELECT SUBSTRING(`timestamp_metric`,6,5) FROM `metric` WHERE SUBSTRING(`timestamp_metric`,6,5) = SUBSTRING(`timestamp_metric`,6,5) GROUP BY SUBSTRING(`timestamp_metric`,6,5) ORDER BY SUBSTRING(`timestamp_metric`,6,5) DESC LIMIt 2');
        //$sql = $this->db->query('SELECT capteur_id,temperature FROM gardom_client.metriques WHERE capteur_id = (SELECT DISTINCT capteur_id FROM gardom_client.metriques LIMIT 1)');
        return $sql->result_array();
    }

    public function get_lastdata()
    {
        $sql = $this->db->query('SELECT * FROM `metric` ORDER BY id_metric DESC LIMIt 1 ');
        //$sql = $this->db->query('SELECT capteur_id,temperature FROM gardom_client.metriques WHERE capteur_id = (SELECT DISTINCT capteur_id FROM gardom_client.metriques LIMIT 1)');
        return $sql->result_array();
    }


    public function get_temperature_7j()
    {
        $sql = $this->db->query('SELECT SUBSTRING(avg(`temperature_metric`),1,4), SUBSTRING(`timestamp_metric`,6,5) FROM `metric` WHERE SUBSTRING(`timestamp_metric`,6,5) = SUBSTRING(`timestamp_metric`,6,5) GROUP BY SUBSTRING(`timestamp_metric`,6,5)  ORDER BY SUBSTRING(`timestamp_metric`,6,5) DESC LIMIt 7');
        //$sql = $this->db->query('SELECT capteur_id,temperature FROM gardom_client.metriques WHERE capteur_id = (SELECT DISTINCT capteur_id FROM gardom_client.metriques LIMIT 1)');
        return $sql->result_array();
    }

    public function plants_by_sensor()
    {
        $sql = $this->db->query('SELECT id_gardom_plant,emplacement_sensor, name_plant FROM avoir,plant,sensor WHERE avoir.id_sensor = sensor.id_sensor AND avoir.id_plant = plant.id_plant');
        //$sql = $this->db->query('SELECT capteur_id,temperature FROM gardom_client.metriques WHERE capteur_id = (SELECT DISTINCT capteur_id FROM gardom_client.metriques LIMIT 1)');
        return $sql->result_array();
    }
    

    public function disk_totalspace($dir = DIRECTORY_SEPARATOR)
    {
        return disk_total_space($dir);
    }


    public function disk_freespace($dir = DIRECTORY_SEPARATOR)
    {
        return disk_free_space($dir);
    }


    public function disk_usespace($dir = DIRECTORY_SEPARATOR)
    {
        return $this->disk_totalspace($dir) - $this->disk_freespace($dir);
    }


    public function disk_freepercent($dir = DIRECTORY_SEPARATOR, $display_unit = FALSE)
    {
        if ($display_unit === FALSE)
        {
            $unit = NULL;
        }
        else
        {
            $unit = ' %';
        }

        return round(($this->disk_freespace($dir) * 100) / $this->disk_totalspace($dir), 0).$unit;
    }


    public function disk_usepercent($dir = DIRECTORY_SEPARATOR, $display_unit = FALSE)
    {
        if ($display_unit === FALSE)
        {
            $unit = NULL;
        }
        else
        {
            $unit = ' %';
        }

        return round(($this->disk_usespace($dir) * 100) / $this->disk_totalspace($dir), 0).$unit;
    }


    public function memory_usage()
    {
        return memory_get_usage();
    }


    public function memory_peak_usage($real = TRUE)
    {
        if ($real)
        {
            return memory_get_peak_usage(TRUE);
        }
        else
        {
            return memory_get_peak_usage(FALSE);
        }
    }


    public function memory_usepercent($real = TRUE, $display_unit = FALSE)
    {
        if ($display_unit === FALSE)
        {
            $unit = NULL;
        }
        else
        {
            $unit = ' %';
        }

        return round(($this->memory_usage() * 100) / $this->memory_peak_usage($real), 0).$unit;
    }
}
