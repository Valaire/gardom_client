<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Capteurs_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }


    public function get_capteurs()
    {
        $sql = $this->db->query('SELECT * FROM gardom_client.sensor');

        return $sql->result_array();
    }

    public function get_capteurs_id()
    {
        $sql = $this->db->query('SELECT id_sensor FROM gardom_client.sensor');

        return $sql->result_array();
    }

    public function update_capteur($capteur_id,$capteur_emplacement)
    {
        //$sql = $this->db->query('UPDATE sensor SET emplacement_sensor = '.$capteur_emplacement.' WHERE id_sensor = '.$capteur_id.'');
        $this->db->set('emplacement_sensor',$capteur_emplacement)->where('id_sensor', $capteur_id)->update('sensor');

    
    }

    public function capteur($id)
	{
		//$this->trigger_events('user');

		// if no id was passed use the current users id
        //$id = isset($id) ? $id : $this->session->userdata('user_id');
        $sql = $this->db->query('SELECT id_sensor FROM gardom_client.sensor WHERE id_sensor = '.$id.'');

        return $sql->result_array();

		/*$this->limit(1);
		$this->order_by($this->tables['users'].'.id', 'desc');
		$this->where($this->tables['users'].'.id', $id);

		$this->users();*/

		return $capteur_id;
	}

}