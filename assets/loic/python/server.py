import socket
import json
import datetime
from mysql.connector import MySQLConnection, Error
from configparser import ConfigParser

def read_db_config(filename='config.ini', section='mysql'):
    parser = ConfigParser()
    parser.read(filename)
    db = {}

    if parser.has_section(section):
        items = parser.items(section)
        for item in items:
            db[item[0]] = item[1]
    else:
        raise Exception('{0} not found in the {1} file'.format(section, filename))

    return db

def dbQuery(query, word):

    dbconfig = read_db_config()
    conn = MySQLConnection(**dbconfig)
    cursor = conn.cursor()

    try:
        cursor.execute(query)
        if word == 'select':
            results = []
            # Should do a list comprehension
            for row in cursor:
                results.append(row)
            return results
        elif word == 'insert' or word == 'update':
            cursor.execute('COMMIT')

    except Error as error:
        print(error)

    finally:
        cursor.close()
        conn.close()

def insertMetrics(timestamp,temperature,humidite,capteur_id):
    query = 'INSERT INTO metriques (timestamp,temperature,humidite,capteur_id) ' \
            'VALUES (\'{0}\',\'{1}\',\'{2}\',\'{3}\');'.format(timestamp,temperature,humidite,capteur_id)
    dbQuery(query, 'insert')

def insertSensor(capteur_id,nom_capteur,type_capteur):
    query = 'INSERT INTO capteurs (id, nom_capteur, type_capteur, keepalive) ' \
            'VALUES ({0},\'{1}\',\'{2}\',true);'.format(capteur_id,nom_capteur,type_capteur)
    dbQuery(query, 'insert')

def selectSensor(capteur_id):
    query = 'SELECT id from capteurs WHERE id = {0};'.format(capteur_id)
    result = dbQuery(query, 'select')
    if len(result) != 0:
        return result[0][0]

def setKeepalive(id,keepalive):
    query = 'UPDATE capteurs SET keepalive = {1} ' \
            'WHERE id = {0};'.format(id,keepalive)
    dbQuery(query, 'update')

def sensorSentry():
    getSensorsIds = 'SELECT id FROM capteurs'

    try:
        sensorIds = dbQuery(getSensorsIds, 'select')

        for id in sensorIds:
            isSensorOnlineQuery = 'SELECT DISTINCT capteur_id FROM metriques WHERE ' \
                                'capteur_id = {0} AND timestamp > NOW() - INTERVAL 1 MINUTE'.format(id[0])
            isSensorOnline = dbQuery(isSensorOnlineQuery,'select')

            if len(isSensorOnline) == 0:
                print('Le capteur {0} est offline'.format(id[0]))
                setKeepalive(id[0],False)

    except Error as errors:
        print(error)

def main():
    s = socket.socket()
    s.settimeout(10)
    s.bind(('0.0.0.0', 10000 ))
    s.listen(0)

    while True:
        sensorSentry()
        decoded=""

        try:
            client, addr = s.accept()

        except KeyboardInterrupt:
            break

        except:
            print('nothing received from the ESP')
            continue

        while True:
            content = client.recv(4096)
            if len(content) ==0:
                break

            else:
                decoded += content.decode('utf-8')

        sensor_metrics = json.loads(decoded)
        timestamp = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

        # Find a way to prettify this block
        temperature = sensor_metrics['ht_sensor_metrics']['temperature']
        humidite = sensor_metrics['ht_sensor_metrics']['humidity']
        capteur_id = sensor_metrics['ht_sensor_metrics']['id']
        nom_capteur = sensor_metrics['ht_sensor_metrics']['sensor_name']
        type_capteur = sensor_metrics['ht_sensor_metrics']['sensor_type']


        print(timestamp)
        print(temperature)
        print(humidite)

        # testId = 10005
        if not selectSensor(capteur_id):
            insertSensor(capteur_id,nom_capteur,type_capteur)
            # insertSensor(10005,'test','test')

        # print(capteur_id)
        # print(nom_capteur)
        # print(type_capteur)

        # insertMetrics(timestamp,temperature,humidite,10002)
        insertMetrics(timestamp,temperature,humidite,capteur_id)
        setKeepalive(capteur_id,True)

        client.close()

main()
