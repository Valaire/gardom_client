const START_DATE1 = "08/01/2019";
const END_DATE1 = "09/01/2019";

$('input[name="daterange"]').daterangepicker(
  {
    startDate: START_DATE1,
    endDate: START_DATE1
  },
  function(start, end) {
    drawChart(start, end);
  }
);
const cubejsApi1 = cubejs(
  "e0c8e361f914be511a2f90cffc630f6c106b4c78ad085de880f97439240071fd3f6d386f5b470498fec010db47efa52c3251ee2676a7955cf9381cd93340b1a9",
  { apiUrl: "http://localhost:4000/cubejs-api/v1" }
);

// A helper method to format data for Chart.js
// and add some nice colors
var chartJsData = function(resultSet) {
  return {
    datasets: [
      {
        label: "Metric1 Count",
        data: resultSet.chartPivot().map(function(r) {
          return r["Metric1.count"];
        }),
        backgroundColor: "rgb(255, 99, 132)"
      }
    ],
    labels: resultSet.categories().map(function(c) {
      return c.x;
    })
  };
};
var options = {
  scales: {
    xAxes: [
      {
        type: "time",
        time: {
          displayFormats: {
            hour: "MMM DD"
          },
          tooltipFormat: "MMM D"
        }
      }
    ],
    yAxes: [
      {
        ticks: {
          beginAtZero: true
        }
      }
    ]
  }
};
var drawChart = function(startDate, endDate) {
  cubejsApi1
    .load({
      measures: ["Metric1.count"],
      timeDimensions: [
        {
          dimension: "Metric1.timestampMetric",
          granularity: `day`,
          dateRange: [startDate, endDate]
        }
      ]
    })
    .then(resultSet => {
      var data = chartJsData(resultSet);
      if (window.chart) {
        window.chart.data = data;
        window.chart.update();
      } else {
        window.chart = new Chart(document.getElementById("chart-canvas1"), {
          type: "line",
          options: options,
          data: data
        });
      }
    });
};

drawChart(START_DATE1, START_DATE1);
