const START_DATE = "01/04/2020";
const END_DATE = "01/09/2020";

$('input[name="daterange"]').daterangepicker(
  {
    startDate: START_DATE,
    endDate: END_DATE
  },
  function(start, end) {
    drawChart(start, end);
  }
);
const cubejsApi = cubejs(
  "6c7e0029207ba06e2d692c9ced772d2bb2d427894800520e158682c055f2a0949c219eb769486b7c467cd2614a2c84aa3c1776401c31a935ad4f6a6507208428",
  { apiUrl: "http://localhost:4000/cubejs-api/v1" }
);

// A helper method to format data for Chart.js
// and add some nice colors
var chartJsData = function(resultSet) {
  return {
    datasets: [
      {
        label: "Metric Count",
        data: resultSet.chartPivot().map(function(r,n) {
          return r["Metric.count"];
        }),
        backgroundColor: "rgb(255, 99, 132)"
      }
    ],
    labels: resultSet.categories().map(function(c) {
      return c.x;
    })
  };
};
var options = {
  scales: {
    xAxes: [
      {
        type: "time",
        time: {
          displayFormats: {
            hour: "MMM DD"
          },
          tooltipFormat: "MMM D"
        }
      }
    ],
    yAxes: [
      {
        ticks: {
          beginAtZero: true
        }
      }
    ]
  }
};
var drawChart = function(startDate, endDate) {
  cubejsApi
    .load({
      measures: ["Metric.count"],
      timeDimensions: [
        {
          dimension: "Metric.timestampMetric",
          granularity: `day`,
          dateRange: [startDate, endDate]
        }
      ]
    })
    .then(resultSet => {
      var data = chartJsData(resultSet);
      if (window.chart) {
        window.chart.data = data;
        window.chart.update();
      } else {
        window.chart = new Chart(document.getElementById("chart-canvas"), {
          type: "line",
          options: options,
          data: data
        });
      }
    });
};

drawChart(START_DATE, END_DATE);
